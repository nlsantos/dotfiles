SH_CUSTOM_ALIASES="$OSH_CUSTOM/aliases/$(hostname).aliases.sh"

if [ -f "${SH_CUSTOM_ALIASES}" ]; then
  source "${SH_CUSTOM_ALIASES}"
fi
