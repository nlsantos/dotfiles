alias gcloud="podman run -e GOOGLE_CLOUD_PROJECT --rm --volumes-from gcloud-config gcr.io/google.com/cloudsdktool/google-cloud-cli:alpine gcloud"
alias bq="podman run -e GOOGLE_CLOUD_PROJECT --rm --volumes-from gcloud-config gcr.io/google.com/cloudsdktool/google-cloud-cli:alpine bq"
