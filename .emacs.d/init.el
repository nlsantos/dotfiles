(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(byte-compile-warnings
   '(redefine callargs free-vars unresolved noruntime interactive-only make-local mapcar constants suspicious lexical lexical-dynamic docstrings docstrings-non-ascii-quotes not-unused))
 '(custom-safe-themes
   '("b9e9ba5aeedcc5ba8be99f1cc9301f6679912910ff92fdf7980929c2fc83ab4d" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default))
 '(delete-selection-mode nil)
 '(display-line-numbers t)
 '(display-line-numbers-current-absolute t)
 '(display-line-numbers-grow-only t)
 '(display-line-numbers-widen t)
 '(display-line-numbers-width 5)
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries 'right)
 '(indicate-empty-lines nil)
 '(inhibit-startup-screen t)
 '(make-backup-files nil)
 '(menu-bar-mode nil)
 '(mini-modeline-enhance-visual t)
 '(mini-modeline-face-attr '(:background "#161616"))
 '(mini-modeline-hide-mode-line t)
 '(mini-modeline-r-format
   '("%e" "%4C" mode-line-mule-info mode-line-client mode-line-modified mode-line-remote mode-line-frame-identification mode-line-buffer-identification " " evil-mode-line-tag
     (:eval
      (string-trim
       (format-mode-line mode-line-modes)))
     mode-line-misc-info))
 '(mini-modeline-right-padding 1)
 '(overflow-newline-into-fringe t)
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("nongnu" . "https://elpa.nongnu.org/nongnu/")
     ("melpa" . "https://melpa.org/packages/")))
 '(package-selected-packages
   '(git-modes windswap nezburn-theme crux bind-key blacken direnv dockerfile-mode eglot-signature-eldoc-talkative nix-mode gh-md zenburn-theme which-key lsp-ui yaml yaml-mode lsp-ivy flx counsel flymake-shellcheck smart-mode-line mini-modeline magit lsp-pyright company-terraform company rainbow-delimiters no-littering ligature lsp-mode))
 '(savehist-mode t)
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(warning-suppress-types '((direnv))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(line-number ((t (:foreground "#2c2c2c" :background "#161616"))))
 '(mini-modeline-mode-line ((t (:background "#d0bf72" :box nil :height 0.1))))
 '(minibuffer-prompt ((t (:foreground "#e3c369"))))
 '(mode-line ((t (:box (:line-width (1 . -1) :style released-button) :foreground "#699869" :background "#111111"))))
 '(mode-line-buffer-id ((t (:weight bold :foreground "#e3c369" :inherit fixed-pitch))))
 '(mode-line-emphasis ((t (:weight bold :inherit fixed-pitch))))
 '(mode-line-highlight ((t (:box (:line-width (2 . 2) :color "grey40" :style released-button) :inherit fixed-pitch)))))

(unless (file-directory-p "~/.emacs.d/elpa")
  (load-file "~/.emacs.d/fresh-start.el")
  (package-initialize)
  (package-reinstall-selected-packages))
(use-package no-littering)

(setq treesit-language-source-alist
      '((markdown "https://github.com/tree-sitter-grammars/tree-sitter-markdown")
        (python "https://github.com/tree-sitter-grammars/tree-sitter-python")
        (toml "https://github.com/tree-sitter-grammars/tree-sitter-toml")
        (yaml "https://github.com/tree-sitter-grammars/tree-sitter-yaml")))

(load-theme 'nezburn t)
(setq nezburn-use-variable-pitch t)
(setq nezburn-scale-org-headlines t)
(setq nezburn-scale-outline-headlines t)
;; (load-theme 'zenburn t)
;; (setq zenburn-use-variable-pitch t)
;; (setq zenburn-scale-org-headlines t)
;; (setq zenburn-scale-outline-headlines t)

(defun load-directory (dir)
  (let ((load-it (lambda (f)
		   (load-file (concat (file-name-as-directory dir) f)))))
    (mapc load-it (directory-files dir nil "\\.el$"))))
(load-directory "~/.emacs.d/user")

(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
