(defun package-reinstall-selected-packages ()
  "Refresh and reinstall previously selected packages. Intended for fresh installs sourcing .dotfiles."
  (package-refresh-contents)
  (dolist (package-name package-selected-packages)
    (unless (package-installed-p package-name)
      (package-install package-name))))
