;; Use gh-md to render Markdown files
(setq markdown-open-command (lambda () (gh-md-render-buffer)))
(add-hook 'gfm-mode-hook
          (lambda ()
            (visual-line-mode 1)
            (markdown-toggle-fontify-code-blocks-natively)))
