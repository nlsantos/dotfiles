;;(setq sml/theme 'light-powerline)
(sml/setup)
(setq mini-modeline-enhance-visual t)
(use-package mini-modeline
  :after smart-mode-line
  :config
  (mini-modeline-mode t))
