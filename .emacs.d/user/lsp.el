(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-semantic-tokens-enable t)
  (setq lsp-semantic-tokens-honor-refresh-requests t)
  :hook ((lsp-mode . lsp-enable-which-key-integration)))

(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)

(use-package lsp-ui
  :init
  (setq lsp-ui-peek-enable t)
  (setq lsp-ui-peek-show-directory t)
  :commands lsp-ui-mode)
