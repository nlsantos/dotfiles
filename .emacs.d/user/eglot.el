(use-package eglot
  :hook (prog-mode . eglot-ensure)
  :bind (("M-TAB" . completion-at-point)
         ("M-g i" . imenu)
         ("C-h ." . display-local-help)
         ("M-." . xref-find-definitions)
         ("M-," . xref-go-back)
         :map
         eglot-mode-map
         ("C-c c a" . eglot-code-actions)
         ("C-c c o" . eglot-code-actions-organize-imports)
         ("C-c c r" . eglot-rename)
         ("C-c c f" . eglot-format)))
