;; Unbind suspend
(global-unset-key [(control z)])

(windswap-default-keybindings 'control 'shift)
(require 'bind-key)
;; crux sanity binds
(bind-keys
 ("C-<backspace>" . crux-kill-line-backwards)
 ("C-<return>" . crux-smart-open-line)
 ("C-c D" . crux-delete-file-and-buffer)
 ("C-c j" . crux-top-join-line)
 ("C-c n" . crux-cleanup-buffer-or-region)
 ("C-c k" . crux-kill-other-buffers)
 ("C-c r" . crux-rename-file-and-buffer)
 ((kbd "S-<down>") . windmove-down)
 ((kbd "S-<left>") . windmove-left)
 ((kbd "S-<right>") . windmove-right)
 ((kbd "S-<up>") . windmove-up)
 ([remap kill-whole-line] . crux-kill-whole-line)
 ([remap move-beginning-of-line] . crux-move-beginning-of-line))
