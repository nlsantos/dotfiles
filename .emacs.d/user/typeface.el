;; (set-face-attribute 'default nil :weight 'regular :font "Comic Sans-13")
;; (set-face-attribute 'italic nil :slant 'italic :weight 'bold :font "Victor Mono-13")
(set-face-attribute 'default nil :weight 'regular :font "Input Sans Condensed-13")
(set-face-attribute 'italic nil :slant 'italic :weight 'bold :font "Victor Mono-13")

(set-face-attribute 'fixed-pitch nil :font "Go Mono-13")
(set-face-attribute 'variable-pitch nil :font "IBM Plex Sans Condensed-13")

(set-face-attribute 'font-lock-comment-face nil :font (face-attribute 'italic :font))
(set-face-attribute 'font-lock-comment-delimiter-face nil :font (face-attribute 'italic :font))
(set-face-attribute 'font-lock-keyword-face nil :font (face-attribute 'italic :font))
