;; (add-to-list 'load-path "~/.emacs.d/lsp-docker")
;; (require 'lsp-docker)
;; (setq lsp-docker-log-docker-supplemental-calls t)
;; (lsp-docker-init-clients
;;  :path-mappings '(("/home/nsantos/Workspace" . "/workspace"))
;;  :client-packages '(lsp-bash lsp-pylsp lsp-terraform)
;;  :client-configs '((:server-id bash-ls
;; 			                   :docker-server-id lspserver-bash
;; 			                   :server-command "bash-language-server start")
;; 		           (:server-id pyright
;;                                :docker-image-id "lspcontainers/pyright-langserver"
;; 			                   :docker-container-name "lsp-pyright"
;; 			                   :server-command "pyright-langserver --stdio")))
