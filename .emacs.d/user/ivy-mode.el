(ivy-mode)
(setq ivy-initial-inputs-alist nil)
(setq ivy-re-builders-alist
      '((ivy-switch-buffer . ivy--regex-plus)
	(t . ivy--regex-fuzzy)))
(setq ivy-use-virtual-buffers t)
(setq ivy-use-selectable-prompt t)
(counsel-mode)
