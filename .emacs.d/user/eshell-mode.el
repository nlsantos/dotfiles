(add-hook 'eshell-mode-hook
  '(lambda () (define-key eshell-mode-map (kbd "<tab>") 'completion-at-point)))
