# Enable the subsequent settings only in interactive sessions
case $- in
  *i*) ;;
    *) return;;
esac

export OSH="$HOME/.oh-my-bash"

OSH_CUSTOM="$HOME/.oh-my-bash-custom"
OSH_THEME="sexy"

OMB_DEFAULT_ALIASES="check"
OMB_HYPHEN_SENSITIVE="true"
OMB_PROMPT_SHOW_PYTHON_VENV=true
OMB_USE_SUDO=false

COMPLETION_WAITING_DOTS="true"

# DISABLE_UNTRACKED_FILES_DIRTY="true"
# SCM_GIT_DISABLE_UNTRACKED_DIRTY="true"
# SCM_GIT_IGNORE_UNTRACKED="true"

completions=(git ssh)
plugins=(git bashmarks)

source "${OSH}/oh-my-bash.sh"

export HISTFILE="${HOME}/.histfiles/histfile.${WINDOW:-base}"
export HISTSIZE=1000

alias cd=pushd

if direnv version &> /dev/null; then
  eval "$(direnv hook bash)"
fi
